import requests
import time
import traceback


with open("webhook_url.txt") as fp:
    data = fp.read()
WEBHOOK_URL = data.strip()


class Notifier:
    def __init__(self):
        self.current_ip = None

    def notify_new_ip(self, new_ip):
        requests.post(
            WEBHOOK_URL,
            json={
                "embeds": [
                    {
                        "title": "IP ADDRESS HAS CHANGED",
                        "description": f"Updated IP Address: `{new_ip}`",
                        "color": 0xec6761
                    }
                ]
            },
        )

    def notify_fetch_ip_error(self, message):
        resp = requests.post("https://hasteb.in/documents", data=message)
        data = resp.json()

        requests.post(
            WEBHOOK_URL,
            json={
                "embeds": [
                    {
                        "title": "An error occurred while fetching IP",
                        "description": f"**https://hasteb.in/{data['key']}**",
                        "color": 0xec6761
                    }
                ]
            },
        )

    def check_ip(self):
        resp = requests.get("http://ifconfig.me")
        resp.raise_for_status()
        curr_ip = resp.text.strip()
        if curr_ip != self.current_ip:
            self.notify_new_ip(curr_ip)
            self.current_ip = curr_ip

    def run(self):
        while True:
            print("Checking IP")
            try:
                self.check_ip()
            except Exception as ex:
                print("An error occurred")
                ex_msg = "".join(traceback.format_exception(type(ex), ex, ex.__traceback__))
                self.notify_fetch_ip_error(ex_msg)
            finally:
                print("IP checking complete")
                time.sleep(60 * 60)
