FROM python:3.8
COPY . /notifier
WORKDIR /notifier
RUN pip install -U pip && pip install -Ur requirements.txt
CMD python -m notifier
